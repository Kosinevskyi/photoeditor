//
//  Photo.swift
//  PhotoEditor
//
//  Created by Roman Kosinevskyi on 9/22/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class PhotoModel {
    //MARK: Variables
    public var photo: UIImage?
    
    static var empty = PhotoModel(photo: nil)
    
    //MARK: Constructor
    init(photo: UIImage?) {
        self.photo = photo
    }
}
