//
//  PhotoEditView.swift
//  PhotoEditor
//
//  Created by Roman Kosinevskyi on 9/22/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class PhotoEditView: UIView {

    @IBOutlet weak var photoImageView: UIImageView!
    
    @IBOutlet weak var watermarkBtn: UIButton!
    
    @IBOutlet weak var saveImageBtn: UIButton!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    deinit {
        print("deinit PhotoEditView")
    }
}
