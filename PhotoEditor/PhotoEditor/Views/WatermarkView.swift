//
//  WatermarkView.swift
//  PhotoEditor
//
//  Created by Roman Kosinevskyi on 9/24/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class WatermarkView: UIView {

    @IBOutlet weak var photoView: UIView!
    @IBOutlet var photoImageView: UIImageView!
    @IBOutlet var watermarkPosition: UIPickerView!
    
    deinit {
        print("deinit WatermarkView!")
    }
}
