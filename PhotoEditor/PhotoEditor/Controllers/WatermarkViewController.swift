//
//  WatermarkViewController.swift
//  PhotoEditor
//
//  Created by Roman Kosinevskyi on 9/16/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit

class WatermarkViewController: UIViewController {
    
    //MARK: Enum
    enum PhotoPositionEnum {
        case LeftTop
        case RightTop
        case LeftBottom
        case RightBottom
    }
    
    //MARK: Property and Vars
    private var photoEditView: WatermarkView! {
        guard isViewLoaded else { return nil }
        return (view as! WatermarkView)
    }
    
    let arrayForPicker = ["LeftTop", "RightTop", "LeftBottom", "RightBottom"]
    
    var photoModel: PhotoModel!
    
    private var photoPosition = PhotoPositionEnum.LeftTop
    //private var tmpWatermark: UIImage?
    private var watermarkOffset = 0
    
    
    
    let watermarkName = "Watermark"
    var watermarkImage: UIImage!
    var watermarkImageView: UIImageView!
    
    var watermarkOffsetX: CGFloat {
        return watermarkImageView.frame.size.width / 2
    }
    
    var watermarkOffsetY: CGFloat {
        return watermarkImageView.frame.size.height / 2
    }
    
    var panGesture  = UIPanGestureRecognizer()
    var pinchGesture = UIPinchGestureRecognizer()
    
    //MARK: Override
    deinit {
        print("deinit WatermarkViewController!")
    }
    
    @objc func dragImg(_ sender:UIPanGestureRecognizer){
        guard let recognizerView = sender.view else {
            return
        }
      
        let boundingRect = photoEditView.photoView.bounds.insetBy(dx: watermarkOffsetX , dy: watermarkOffsetY)
        
        let translation = sender.translation(in: photoEditView.photoView)
        var targetCenter = recognizerView.center
        
        targetCenter.x += translation.x
        if boundingRect.contains(targetCenter) {
            recognizerView.center.x += translation.x
        }
        
        targetCenter.x -= translation.x
        targetCenter.y += translation.y
        if boundingRect.contains(targetCenter) {
            recognizerView.center.y += translation.y
        }
        
        sender.setTranslation(.zero, in: view)
    }
    
    //Pinch Gesture for zoom in and zoom out
    @IBAction func scaleImg(_ sender: UIPinchGestureRecognizer) {
        watermarkImageView.transform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        watermarkImage = UIImage(named: watermarkName)
        watermarkImageView = UIImageView(image: watermarkImage!)
        
        watermarkImageView.frame = CGRect(x: 100, y: 100, width: 200, height: 200)
        photoEditView.photoView.addSubview(watermarkImageView)
        
        
        //pan gesture for dragging an image
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(dragImg(_:)))
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(scaleImg(_:)))
        watermarkImageView.isUserInteractionEnabled = true
        watermarkImageView.addGestureRecognizer(panGesture)
        watermarkImageView.addGestureRecognizer(pinchGesture)

        photoEditView.watermarkPosition.dataSource = self as UIPickerViewDataSource
        photoEditView.watermarkPosition.delegate = self as UIPickerViewDelegate

        photoEditView.photoImageView.image = photoModel.photo
        
        navigationController?.topViewController?.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveNavigationVCBtn))
        
        guard let tmpImage = photoModel.photo else {
            print("guard photoModel.photo is NIL")
            return
        }
        
        watermarkOffset = Int((tmpImage.size.width * 1.5) / 100)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("WatermarkViewController viewWillDisappear")
    }
    
    //MARK: IBAction
    @IBAction func selectWatermarkBtn(_ sender: Any) {
    }
    
    @objc func saveNavigationVCBtn() {
        print("User pressed SAVE!")
        
        photoEditView.photoImageView.image = drawLogoIn2(photoEditView.photoImageView.image!, watermarkImageView.image!, position: CGPoint(x: watermarkImageView.frame.minX ,y: watermarkImageView.frame.minY))
        
        photoModel.photo = photoEditView.photoImageView.image
        navigationController?.popViewController(animated: true)
    }
    
    //MARK: Func
    func changePosition(position: PhotoPositionEnum) {
        var pos: (x: CGFloat, y: CGFloat) = (0, 0)
        
        switch position {
        case .LeftTop:
            pos.x = watermarkOffsetX
            pos.y = watermarkOffsetY
            
        case .RightTop:
            pos.x = photoEditView.photoView.frame.size.width - watermarkOffsetX
            pos.y = watermarkOffsetY
            
        case .LeftBottom:
            pos.x = watermarkOffsetX
            pos.y = photoEditView.photoView.frame.size.height - watermarkOffsetY
        case .RightBottom:
            pos.x = photoEditView.photoView.frame.size.width - watermarkOffsetX - 1
            pos.y = photoEditView.photoView.frame.size.height - watermarkOffsetY - 1
        }
        
        watermarkImageView.center.x = pos.x
        watermarkImageView.center.y = pos.y
    }
    
    //MARK: Test
    private func drawLogoIn2(_ image: UIImage, _ logo: UIImage, position: CGPoint) -> UIImage {
        //просчет процентажа
        let percent = (watermarkImageView.frame.size.width * 100 ) / photoEditView.photoImageView.frame.size.width
        
        let size = image.size
        UIGraphicsBeginImageContext(size)
        
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        image.draw(in: areaSize)
        
        let offsX = (size.width * position.x) / photoEditView.photoImageView.frame.size.width
        let offsY = (size.height * position.y) / photoEditView.photoImageView.frame.size.height
        
        let logoS = (size.width * percent) / 100
        let logoSize = CGRect(x: offsX, y: offsY, width: logoS, height: logoS)
        logo.draw(in: logoSize, blendMode: .normal, alpha: 1)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
}

extension WatermarkViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    //MARK: Func UIPicker
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayForPicker.count
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let result = arrayForPicker[row]
        
        return result
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch arrayForPicker[row] {
        case "LeftTop":
            photoPosition = .LeftTop
        case "RightTop":
            photoPosition = .RightTop
        case "LeftBottom":
            photoPosition = .LeftBottom
        case "RightBottom":
            photoPosition = .RightBottom
        default:
            print("Error")
        }
        
        changePosition(position: photoPosition)
    }
}
