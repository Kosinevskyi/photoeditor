//
//  ViewController.swift
//  PhotoEditor
//
//  Created by Roman Kosinevskyi on 9/8/19.
//  Copyright © 2019 Roman Kosinevskyi. All rights reserved.
//

import UIKit
import PixelEngine
import PixelEditor

protocol ImagePickingProtocol: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func presentImagePicker()
}

extension ImagePickingProtocol where Self: UIViewController {
    func presentImagePicker() {
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .photoLibrary
        
        present(picker, animated: true, completion: nil)
    }
}

class PhotoEditViewController: UIViewController {
    
    //MARK: Property
    private var photoEditView: PhotoEditView! {
        guard isViewLoaded else { return nil }
        return (view as! PhotoEditView)
    }
    
    public var photoModel: PhotoModel! = .empty
    
    //MARK: Override
    deinit {
        print("deinit PhotoEditViewController")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("PhotoEditViewController viewWillAppear!")
        photoEditView.photoImageView.image = photoModel.photo
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        photoEditView.loading.stopAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WatermarkVC", let watermarkVC = segue.destination as? WatermarkViewController {
            //watermarkVC.rootImage = photoEditView.photoImageView.image
            watermarkVC.photoModel = photoModel
        }
    }
    
    //MARK: IBAction
    @IBAction func selectImageBtn(_ sender: UIButton) {
        let picker = UIImagePickerController()
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .photoLibrary
        
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func saveImageBtn(_ sender: UIButton) {
        //guard let image = photoEditView.photoImageView.image else {
        guard let image = photoModel.photo else {
            print("Need download picture!")
            return
        }
        
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    //MARK: Func
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Your altered image has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    private func updateUIPhoto() {
        photoEditView.photoImageView.image = photoModel.photo
    }
    
    private func showExtraFunctionality() {
        photoEditView.watermarkBtn.isHidden = false
        photoEditView.saveImageBtn.isHidden = false
    }
}

extension PhotoEditViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: Func
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        photoEditView.loading.startAnimating()
        
        let selectedImage = info[.originalImage] as? UIImage
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = selectedImage else {
            print("Need download picture!")
            return
        }
        
        let controller = PixelEditViewController.init(image: image)
        
        controller.delegate = self
        
        navigationController?.pushViewController(controller, animated: true)
    }
}

extension PhotoEditViewController : PixelEditViewControllerDelegate {
    //MARK: Func
    func pixelEditViewController(_ controller: PixelEditViewController, didEndEditing editingStack: EditingStack) {
        self.navigationController?.popToViewController(self, animated: true)
        let image = editingStack.makeRenderer().render(resolution: .full)
        photoModel.photo = image
        updateUIPhoto()
        showExtraFunctionality()
    }
    
    func pixelEditViewControllerDidCancelEditing(in controller: PixelEditViewController) {
        self.navigationController?.popToViewController(self, animated: true)
    }
}
